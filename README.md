This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


# `F1 React`

## Getting Started

To get you started you can simply clone the `f1` repository and install the dependencies:

### Prerequisites

You need git to clone the repository. You can get git from [here][git].

You must have Node.js and its package manager (npm) installed. You can get them from [here][node].

### Clone `f1`

Clone the `f1` repository using git:

```
git clone https://amineatallah@bitbucket.org/amineatallah/f1.git
cd f1
```

### Install Dependencies

```
npm install
```

### Run the Application


```
npm start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


```
npm run build
```

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!


## Folder Structure


```
my-app/                     --> all of the source files for the application.
  public/                   --> app public files.
    images/                 --> app images.
    index.html              --> app layout file (the main html template file of the app)
    favicon.ico
  src/
    actions/                --> application actions.
      actionTypes.js        --> action types constants.
      detailsActions.js     --> details actions creator.
      homeActions.js        --> home actions creator.
      uiActions.js          --> ui actions creator.
    components/             --> app components
      details/              --> details page files.
        detailsPage.css     --> details page css
        DetailsPage.js      --> details page component
        Season.js           --> Season comopnent
      general/              --> general components
        Loader.js           --> loader component
        NavLink.js          --> href component
      home/                 --> home page files.
        home.css            --> home page css.
        Home.js             --> home page component
        WinnerCard.js       --> winnerCard component
    reducers/               --> app reducers.
      index.js              --> index reducer that combine all the reducers.
      seasonReducer.js      --> season reducer.
      uiReducer.js          --> ui reducer.
      winnersReducer.js     --> winnersReducer.
    store/                  --> application store.
      configureStore.js     --> create and configure store.
    App.css                 --> App component css
    App.js                  --> main app component.
    config.js               --> main app config.
    httpConfig.js           --> axios http client default config.
    index.css               --> main css file.
    index.js                --> app boostrap file.
    routes.js               --> routes file
    package.json
    README.md
```

## Framework, libraries and tools used.

* [React](http://www.dropwizard.io/1.0.2/docs/) - A Javascript library for building user interfaces.
* [Redux](http://http://redux.js.org/) - Redux is a predictable state container for JavaScript apps.
* [React-redux](https://maven.apache.org/) - Official React bindings for Redux.
* [Create React App](https://github.com/facebookincubator/create-react-app) - Bootstrap for React apps.

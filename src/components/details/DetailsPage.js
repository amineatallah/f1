import React from 'react';
import {connect} from 'react-redux';
import * as detailsActions from '../../actions/detailsActions';
import Season from './Season';
import './detailsPage.css';

class DetailsPage extends React.Component {

  componentDidMount() {
    this.props.fetchSeason(this.props.params.year);
  }

  componentWillUnmount() {
    this.props.clearSeason();
  }

  render(){
    return (
      <div className="details mdl-grid">
        <h3 className="mdl-cell mdl-cell--7-col">{this.props.params.year} Race Results</h3>
        <div className="mdl-cell mdl-cell--7-col">
          <Season races={this.props.races} winner={this.props.params.winner}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    races: state.season
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchSeason: (year) => dispatch(detailsActions.fetchSeason(year)),
    clearSeason: () => dispatch(detailsActions.clearSeason())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);

import React from 'react';

const Season = (props) => {
  let races = props.races.map((race) => {
    return (
      <tr key={race.id} className={props.winner === race.winnerId ? 'winner' : null} >
        <td className="mdl-data-table__cell--non-numeric">{race.raceName}</td>
        <td className="mdl-data-table__cell--non-numeric mdl-cell--hide-phone mdl-cell--hide-tablet">{race.date}</td>
        <td className="mdl-data-table__cell--non-numeric">{race.winner}</td>
        <td className="mdl-data-table__cell--non-numeric mdl-cell--hide-phone">{race.car}</td>
        <td className="mdl-cell--hide-phone mdl-cell--hide-tablet">{race.laps}</td>
      </tr>
    );
  });

  return (
    <table className="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th className="mdl-data-table__cell--non-numeric">GRAND PRIX</th>
          <th className="mdl-data-table__cell--non-numeric mdl-cell--hide-phone mdl-cell--hide-tablet">Date</th>
          <th className="mdl-data-table__cell--non-numeric">Winner</th>
          <th className="mdl-data-table__cell--non-numeric mdl-cell--hide-phone">Car</th>
          <th className="mdl-cell--hide-phone mdl-cell--hide-tablet">Laps</th>
        </tr>
      </thead>
      <tbody>
        {races}
      </tbody>
    </table>
  )
};

export default Season;

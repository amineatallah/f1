import React from 'react';
import {Link} from 'react-router';

const NavLinks = (props) =>  {
  return <Link {...props} />
}

export default NavLinks;

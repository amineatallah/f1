import React, {PropTypes} from 'react';
import {connect} from 'react-redux';

class Loader extends React.Component {

  componentDidUpdate() {
    // material design light function to rerender the loader
    if(window.componentHandler){
      window.componentHandler.upgradeAllRegistered();
    }
  }

  render() {
   if(this.props.showLoading !== this.props.loadingClass) return null;
    return (
      <div className="mdl-progress mdl-js-progress mdl-progress--indeterminate" />
    );
  }
}

Loader.propTypes = {
  loadingClass: PropTypes.string.isRequired,
  showLoading: PropTypes.string
};

const mapStateToProps = (state) => {
  return {
    showLoading: state.ui.showLoading
  };
};

export default connect(mapStateToProps)(Loader);

import React from 'react';
import NavLinks from '../general/NavLink';

const WinnerCard = ({winner}) => {
  return (
    <div className="home-card mdl-card mdl-shadow--2dp mdl-cell mdl-cell--7-col mdl-cell--middle">
      <div className="mdl-card__title">
        <img src={`/images/${winner.driverId}.jpg`} alt=""/>
        <h2 className="mdl-card__title-text">{winner.season} - {winner.name}</h2>
      </div>
      <div className="mdl-card__supporting-text mdl-grid">
        <span className="mdl-cell mdl-cell--6-col"><b>Nationality:</b> {winner.nationality}</span>
        <span className="mdl-cell mdl-cell--6-col"><b>Points:</b> {winner.points}</span>
        <span className="mdl-cell mdl-cell--6-col"><b>Car:</b> {winner.car}</span>
        <span className="mdl-cell mdl-cell--6-col"><b>Wins:</b> {winner.wins}</span>
      </div>
      <div className="mdl-card__actions mdl-card--border">
        <NavLinks
          to={`/seasons/${winner.season}/${winner.driverId}`}
          className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
          Go To Season
        </NavLinks>
      </div>
    </div>
  );
};

export default WinnerCard;

import React from 'react';
import {connect} from 'react-redux';
import WinnerCard from './WinnerCard';
import * as homeActions from '../../actions/homeActions';
import './home.css';

class Home extends React.Component {

  componentDidMount() {
    this.props.fetchWinnersList();
  }

  render() {
    let winners = this.props.winners.map((winner) => {
      return (
        <WinnerCard key={winner.id} winner={winner}/>
      )
    });
    return (
      <div className="home mdl-grid">
        {winners}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    winners: state.winners
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchWinnersList: () => dispatch(homeActions.fetchWinnersList())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);

import { combineReducers } from 'redux';
import winnersReducer from './winnersReducer';
import seasonReducer from './seasonReducer';
import uiReducer from './uiReducer';

export default combineReducers({
  winners: winnersReducer,
  season: seasonReducer,
  ui: uiReducer
});

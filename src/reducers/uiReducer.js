import * as actionTypes from '../actions/actionTypes';

export default (state = {}, action) => {
  switch(action.type){
    case actionTypes.START_LOADING:
      return {...state, showLoading: 'mdl-progress--indeterminate'};
    case actionTypes.STOP_LOADING:
      return {...state, showLoading: ''};
    default:
      return state;
  }
};

import * as actionTypes from '../actions/actionTypes';

export default ( state = [], action ) => {
    switch(action.type) {
      case actionTypes.FETCH_WINNERS_SUCCESS:
        return action.data;
      default:
        return state;
    }
};

import * as actionTypes from '../actions/actionTypes';

export default (state = [], action) => {
  switch(action.type) {
    case actionTypes.FETCH_SEASON_SUCCESS:
      return action.data;
    case actionTypes.CLEAR_SEASON:
      return [];
    default:
      return state;
  }
};

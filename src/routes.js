import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './App';
import Home from './components/home/Home';
import DetailsPage from './components/details/DetailsPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="/seasons/:year/:winner" component={DetailsPage} />
  </Route>
);

import Axios from 'axios';
import {apiUrl} from './config';

const customAxios = Axios.create({
  baseURL: apiUrl
});

export default customAxios;

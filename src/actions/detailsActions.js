import * as actionTypes from './actionTypes';
import {loading} from './uiActions';
import customAxios from '../httpConfig';

const fetchSeasonSuccess = (season) => {
  return {
    type: actionTypes.FETCH_SEASON_SUCCESS,
    data: season
  };
};

export const fetchSeason = (year) => {
  return (dispatch) => {
    dispatch(loading(actionTypes.START_LOADING));
    return customAxios.get(year + '/results/1.json')
                .then((response) => {
                  dispatch(loading(actionTypes.STOP_LOADING));
                  let races = response.data.MRData.RaceTable.Races;
                  let season = races.map((race, index) => {
                    return {
                      id: (Date.now() + index) * race.Results[0].laps, // create a unique id.
                      raceName: race.raceName,
                      date: new Date(race.date).toDateString(),
                      winnerId: race.Results[0].Driver.driverId,
                      winner: race.Results[0].Driver.givenName + ' ' + race.Results[0].Driver.familyName,
                      car: race.Results[0].Constructor.name,
                      laps: race.Results[0].laps
                    };
                  });
                  dispatch(fetchSeasonSuccess(season));
                })
                .catch((error)=> {
                  console.log(error);
                })
  };
};

export const clearSeason = () => {
  return {
    type: actionTypes.CLEAR_SEASON
  };
};


import * as actionTypes from './actionTypes';
import {loading} from './uiActions';
import customAxios from '../httpConfig';

const fetchWinnersListSuccess = (winners) => {
  return {
    type: actionTypes.FETCH_WINNERS_SUCCESS,
    data: winners
  };
};

export const fetchWinnersList = (limit=11, offset=55) => {
  return (dispatch) => {
    dispatch(loading(actionTypes.START_LOADING));
    return customAxios.get('driverstandings/1.json', {params: {limit, offset}})
                .then((response) => {
                  dispatch(loading(actionTypes.STOP_LOADING));
                  let data = response.data.MRData.StandingsTable.StandingsLists;
                  let winners = data.map((winner, index) => {
                    return {
                      id: (Date.now() + index) * winner.DriverStandings[0].points,
                      season: winner.season,
                      driverId: winner.DriverStandings[0].Driver.driverId,
                      name: winner.DriverStandings[0].Driver.givenName + ' ' + winner.DriverStandings[0].Driver.familyName,
                      nationality: winner.DriverStandings[0].Driver.nationality,
                      points: winner.DriverStandings[0].points,
                      wins: winner.DriverStandings[0].wins,
                      car: winner.DriverStandings[0].Constructors[0].name
                    }
                  });
                  dispatch(fetchWinnersListSuccess(winners));
                })
                .catch((error)=> {
                  console.log(error);
                })
  };
};

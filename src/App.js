import React, { Component } from 'react';
import NavLink from './components/general/NavLink';
import Loader from './components/general/Loader';
import Logo from '../public/images/f1_logo.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <Loader loadingClass="mdl-progress--indeterminate"/>
        <header className="mdl-layout__header">
          <div className="mdl-layout__header-row">
            <span className="mdl-layout__title">
              <NavLink to="/"><img src={Logo} alt="logo" /></NavLink>
            </span>
          </div>
        </header>
        <main className="mdl-layout__content">
          <div className="page-content">
            {this.props.children}
          </div>
        </main>
      </div>
    );
  }
}

export default App;
